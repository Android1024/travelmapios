import UIKit

class AMPersonProfileVM: NSObject {

	dynamic var model :AMPerson?
	let id :Int

	init(id: Int) {
		self.id = id

		super.init()

		self.model = AMPersonProfileVM.findPersonWithId(id, all: AMDataProvider.sharedInstance.getPersonsArray().1)

		NSNotificationCenter.defaultCenter().addObserver(self,
			selector: "personsDataUpdateCompleted:",
			name: AMDataProvider.sharedInstance.personsArrayUpdatedNotificationName,
			object: nil)

		NSNotificationCenter.defaultCenter().addObserver(self,
			selector: "personsDataUpdateFailed:",
			name: AMDataProvider.sharedInstance.personsArrayFailedNotificationName,
			object: nil)
	}

	func personsDataUpdateCompleted(notification: NSNotification) {
		let personsData :[AMPerson]? = notification.object as! ([AMPerson]?)
		self.model = AMPersonProfileVM.findPersonWithId(id, all: personsData)
	}

	func personsDataUpdateFailed(notification: NSNotification) {
	}

	static func findPersonWithId(id :Int, all :[AMPerson]?) -> AMPerson? {
		if let persons = all {
			for person in persons {
				if person.id == id {
					return person
				}
			}
		}

		return nil
	}
}
