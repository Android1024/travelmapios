import UIKit

enum AMCurrentState {
	case Idle
	case Loading
	case Loaded
	case Failed
}

class AMCurrentStateVM: NSObject {

	dynamic var isLoadingState = false
	dynamic var model :AMCurrentStateModel? = nil

	private var loadingTocken :Int = -1

	override init() {
		super.init()

		NSNotificationCenter.defaultCenter().addObserver(self,
			selector: "personsDataUpdateCompleted:",
			name: AMDataProvider.sharedInstance.personsArrayUpdatedNotificationName,
			object: nil)

		NSNotificationCenter.defaultCenter().addObserver(self,
			selector: "personsDataUpdateFailed:",
			name: AMDataProvider.sharedInstance.personsArrayFailedNotificationName,
			object: nil)
	}

	deinit {
		NSNotificationCenter.defaultCenter().removeObserver(self)
	}

	func loadData() {
		let tuple = AMDataProvider.sharedInstance.getPersonsArray()
		let tocken = tuple.0
		let newPersonsData = tuple.1

		if tocken > 0 {
			self.loadingTocken = tocken
			self.updateIsLoadingState()
		}

		self.updateModelWithPersonsData(newPersonsData)
	}

	func personsDataUpdateCompleted(notification: NSNotification) {
		self.extractLoadingTockenAndUpdateLoadingState(notification)
		let personsData :[AMPerson]? = notification.object as! ([AMPerson]?)
		self.updateModelWithPersonsData(personsData)
	}

	func personsDataUpdateFailed(notification: NSNotification) {
		self.extractLoadingTockenAndUpdateLoadingState(notification)
	}

	private func extractLoadingTockenAndUpdateLoadingState(notification: NSNotification) {

		let loadingTocken = notification.userInfo![AMDataProvider.sharedInstance.personsArrayNotificationTockenKey] as? Int
		if let tocken = loadingTocken {
			if self.loadingTocken > 0 && self.loadingTocken == tocken {
				self.loadingTocken = -1
				self.updateIsLoadingState()
			}
		}
	}

	private func updateModelWithPersonsData(data :[AMPerson]?) {
		if let personsData = data {
			let currentPerson = findCurrentPerson(personsData)
			let nearestPerson = findNearestPerson(currentPerson, all: personsData)
			var nearestPersonsArray = [AMPerson]()
			if let nearest = nearestPerson {
				nearestPersonsArray = [nearest]
			}

			self.model = AMCurrentStateModel(coordinates: currentPerson.coordinate,
				nearestPersons: nearestPersonsArray,
				nearestGeoObject: self.model?.nearestGeoObject)
		}
	}

	private func findCurrentPerson(personsArray: [AMPerson]) -> AMPerson {
		for person in personsArray {
			if person.id == AMDataProvider.currentUserHardCodedId {
				return person
			}
		}

		return personsArray.first!
	}

	private func findNearestPerson(currentPerson: AMPerson, all: [AMPerson]) -> AMPerson? {
		var nearestPerson :AMPerson?
		if let _ = currentPerson.coordinate {
			for person in all {
				if let _ = person.coordinate {
					if person.id != currentPerson.id {
						if let nearest = nearestPerson {
							if currentPerson.distanceToPerson(nearest) > currentPerson.distanceToPerson(person) {
								nearestPerson = person
							}
						} else {
							nearestPerson = person
						}
					}
				}
			}
		}

		return nearestPerson
	}

	private func updateIsLoadingState () {
		self.isLoadingState = (self.loadingTocken > 0)
	}



}
