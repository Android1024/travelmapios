import UIKit

class AMMainMapVM: NSObject {

	dynamic var model = AMMainMapModel(personsArray: [AMPerson](), geoObjects: [AMGeoObject]())
	var currentPerson :AMPerson?

	override init() {
		super.init()

		if let personsData = AMDataProvider.sharedInstance.getPersonsArray().1 {
			self.currentPerson = findCurrentPerson(personsData)

			if let coord = self.currentPerson?.coordinate {
				if let geoObjects = AMDataProvider.sharedInstance.getGeoObjectsArray(coord).1 {
					self.model = AMMainMapModel(personsArray: personsData, geoObjects: geoObjects)
				}
			}
		}

		NSNotificationCenter.defaultCenter().addObserver(self,
			selector: "personsDataUpdateCompleted:",
			name: AMDataProvider.sharedInstance.personsArrayUpdatedNotificationName,
			object: nil)

		NSNotificationCenter.defaultCenter().addObserver(self,
			selector: "personsDataUpdateFailed:",
			name: AMDataProvider.sharedInstance.personsArrayFailedNotificationName,
			object: nil)

		NSNotificationCenter.defaultCenter().addObserver(self,
			selector: "geoObjectsDataUpdateCompleted:",
			name: AMDataProvider.sharedInstance.geoObjectsArrayUpdatedNotificationName,
			object: nil)

		NSNotificationCenter.defaultCenter().addObserver(self,
			selector: "geoObjectsDataUpdateFailed:",
			name: AMDataProvider.sharedInstance.geoObjectsArrayFailedNotificationName,
			object: nil)
	}

	deinit {
		NSNotificationCenter.defaultCenter().removeObserver(self)
	}

	func personsDataUpdateCompleted(notification: NSNotification) {
		let personsData :[AMPerson]? = notification.object as! ([AMPerson]?)
		if let persons = personsData {

			self.currentPerson = findCurrentPerson(persons)
			self.model = AMMainMapModel(personsArray: persons, geoObjects :self.model.geoObjects)

			if let coord = self.currentPerson?.coordinate {
				AMDataProvider.sharedInstance.getGeoObjectsArray(coord)
			}
		}
	}

	func personsDataUpdateFailed(notification: NSNotification) {
	}

	func findCurrentPerson(persons :[AMPerson]) -> AMPerson? {
		for person in persons {
			if person.id == AMDataProvider.currentUserHardCodedId {
				return person
			}
		}

		return nil
	}

	func geoObjectsDataUpdateCompleted(notification: NSNotification) {
		let geoObjectsData :[AMGeoObject]? = notification.object as! ([AMGeoObject]?)
		if let geoObjects = geoObjectsData {

			self.model = AMMainMapModel(personsArray: self.model.personsArray, geoObjects: geoObjects)
		}
	}

	func geoObjectsDataUpdateFailed(notification: NSNotification) {
	}

}
