import UIKit

class AMSelfProfileVC: AMPersonProfileVC {

	override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
		super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
		self.navigationController?.title = "Me"
	}

	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		self.navigationController?.title = "Me"
		self.tabBarItem.image = UIImage(named: "ProfileIcon")
	}

    override func viewDidLoad() {
        super.viewDidLoad()

		self.title = "Me"
		self.personId = AMDataProvider.currentUserHardCodedId
    }

}
