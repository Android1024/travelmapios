import UIKit
import ReactiveCocoa
import SnapKit

class AMCurrentStateVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

	let rtpApiWrapper = AMRussiaTravelApiWrapper()
	let tmApiWrapper = AMTravelMapApiWrapper()

	var viewModel = AMCurrentStateVM()

	let cellReuseIdentifier = "AMCurrentStateCell"

	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var activityIndicator: UIActivityIndicatorView!

	override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
		super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
		self.navigationController?.title = "Current state"
	}

	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		self.navigationController?.title = "Current state"
		self.tabBarItem.image = UIImage(named: "CurrentStateIcon")
	}

	override func viewDidLoad() {
		super.viewDidLoad()

		self.title = "Current state"

		tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: cellReuseIdentifier)
		tableView.delegate = self;
		tableView.dataSource = self;

		self.activityIndicator.hidesWhenStopped = true

		viewModel.loadData()

		viewModel.rac_valuesForKeyPath("model",
			observer: viewModel)
			.distinctUntilChanged()
			.subscribeNext {[weak self] (_) -> Void in
				guard let sself = self else {
					return
				}

				sself.modelUpdated()
		}

		viewModel.rac_valuesForKeyPath("isLoadingState",
			observer: viewModel)
			.subscribeNext {[weak self] (_) -> Void in
				guard let sself = self else {
					return
				}

				sself.loadingStateUpdated()
		}
	}

	private func modelUpdated() {
		self.tableView?.reloadData()
	}

	private func loadingStateUpdated() {
		self.tableView.hidden = self.viewModel.isLoadingState

		if self.viewModel.isLoadingState {
			self.activityIndicator.startAnimating()
		} else {
			self.activityIndicator.stopAnimating()
		}
	}

	// MARK: Segue

	override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
		if segue.identifier! == "Profile" {
			let profileVC = segue.destinationViewController as! AMPersonProfileVC
			profileVC.personId = self.viewModel.model?.nearestPersons?.first?.id
		}
	}

	// MARK: TableViewDataSource

	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return 2
	}

	func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
		return 50
	}

	func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCellWithIdentifier(self.cellReuseIdentifier, forIndexPath: indexPath)
		switch indexPath.row {
		case 0:
			if let nearestPerson = self.viewModel.model?.nearestPersons?.first {
				cell.textLabel?.text = "Nearest traveller: \(nearestPerson.firstname) \(nearestPerson.lastname)"
			} else {
				cell.textLabel?.text = "No travellers around"
			}
		case 1:
			if let coordinates = self.viewModel.model?.coordinates {
				cell.textLabel?.text = "Coordinates: \(coordinates.latitude), \(coordinates.longitude)"
			} else {
				cell.textLabel?.text = ""
			}
		default: break
		}

		return cell
	}

	func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		self.tableView.deselectRowAtIndexPath(indexPath, animated: true)
		if indexPath.row == 0 {
			self.performSegueWithIdentifier("Profile", sender: nil)
		}
	}

	// MARK: TableViewDelegate

}

