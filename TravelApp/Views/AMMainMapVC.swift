import UIKit
import MapKit

class AMMainMapPinButton: UIButton {
	var annotation :AMPersonMapAnnotation?
}

class AMMainMapVC: UIViewController, MKMapViewDelegate {

	@IBOutlet weak var mapView: MKMapView!
	@IBOutlet weak var geoObjectsSwitch: UISwitch!
	static let pinAnnotationReuseIdentifier = "MainMapPinAnnotationView"
	static let annotationReuseIdentifier = "MainMapAnnotationView"

	var viewModel = AMMainMapVM()
	var personsAnnotations = [AMPersonMapAnnotation]()
	var geoObjectsAnnotations = [AMGeoObjectMapAnnotation]()

	var lastChoosenPerson :AMPerson?

	var wasFocused = false

	override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
		super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
		self.navigationController?.title = "Map"
	}

	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		self.navigationController?.title = "Map"
		self.tabBarItem.image = UIImage(named: "MapIcon")
	}

    override func viewDidLoad() {
        super.viewDidLoad()

		self.title = "Map"

		self.mapView.delegate = self;

		self.geoObjectsSwitch.tintColor = UIColor.blueColor()
		self.geoObjectsSwitch.onTintColor = UIColor.blueColor()
		self.geoObjectsSwitch.on = false
		self.geoObjectsSwitch.addTarget(self, action: "updatePins", forControlEvents: .ValueChanged)

		viewModel.rac_valuesForKeyPath("model",
			observer: viewModel)
			.distinctUntilChanged()
			.subscribeNext {[weak self] (_) -> Void in
				guard let sself = self else {
					return
				}

				sself.modelUpdated()
		}
    }

	func modelUpdated() {
		self.updatePins()
	}

	func updatePins() {
		guard let _: AMMainMapModel = self.viewModel.model,
			let _: AMPerson = self.viewModel.currentPerson else {
			return
		}

		updatePersonsAnnotation()
		updateGeoObjectsAnnotation()

		if !self.wasFocused {
			self.wasFocused = true
			let region = MKCoordinateRegion(center: self.viewModel.currentPerson!.coordinate!, span: MKCoordinateSpanMake(0.03, 0.03))
			self.mapView.setRegion(region, animated: true)
		}
	}

	func updatePersonsAnnotation() {
		let oldAnnotations = self.personsAnnotations

		var newAnnotations = [AMPersonMapAnnotation]()
		for person in self.viewModel.model.personsArray {
			newAnnotations.append(AMPersonMapAnnotation(person: person))
		}
		self.personsAnnotations = newAnnotations
		self.mapView.removeAnnotations(oldAnnotations)
		self.mapView.addAnnotations(newAnnotations)
	}

	func updateGeoObjectsAnnotation() {
		let oldAnnotations = self.geoObjectsAnnotations

		var newAnnotations = [AMGeoObjectMapAnnotation]()
		for geoObject in self.viewModel.model.geoObjects {
			newAnnotations.append(AMGeoObjectMapAnnotation(geoObject: geoObject))
		}
		self.geoObjectsAnnotations = newAnnotations
		self.mapView.removeAnnotations(oldAnnotations)

		if self.geoObjectsSwitch.on {
			self.mapView.addAnnotations(newAnnotations)
		}
	}

	// MARK: Seque

	override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
		if segue.identifier! == "Profile" {
			let profileVC = segue.destinationViewController as! AMPersonProfileVC
			profileVC.personId = self.lastChoosenPerson?.id
		}
	}

	// MARK: MapViewDelegate

	func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
		var viewForAnnotation :MKAnnotationView?

		if let personAnnotation = annotation as? AMPersonMapAnnotation {

			var pinAnnotation :MKPinAnnotationView?

			pinAnnotation = self.mapView.dequeueReusableAnnotationViewWithIdentifier(AMMainMapVC.pinAnnotationReuseIdentifier) as! MKPinAnnotationView?
			if let _ = pinAnnotation {

			} else {
				pinAnnotation = MKPinAnnotationView(annotation: annotation, reuseIdentifier: AMMainMapVC.pinAnnotationReuseIdentifier)
			}

			let pinView = pinAnnotation!

			pinView.annotation = annotation
			pinView.canShowCallout = true
			pinView.rightCalloutAccessoryView = self.pinRightButton(annotation as! AMPersonMapAnnotation)

			if (personAnnotation.person.id == AMDataProvider.currentUserHardCodedId) {
				pinView.pinColor = .Green
			} else {
				pinView.pinColor = .Red
			}

			viewForAnnotation = pinAnnotation
		} else if let _ = annotation as? AMGeoObjectMapAnnotation {

			viewForAnnotation = self.mapView.dequeueReusableAnnotationViewWithIdentifier(AMMainMapVC.annotationReuseIdentifier)
			if let _ = viewForAnnotation {

			} else {
				viewForAnnotation = MKAnnotationView(annotation: annotation, reuseIdentifier: AMMainMapVC.annotationReuseIdentifier)
			}

			viewForAnnotation?.annotation = annotation
			viewForAnnotation?.canShowCallout = true
			viewForAnnotation?.frame = CGRect(x: 0, y: 0, width: 10, height: 10)
			viewForAnnotation?.backgroundColor = UIColor.whiteColor()
			viewForAnnotation?.layer.cornerRadius = 5
			viewForAnnotation?.layer.borderColor = UIColor.blueColor().CGColor
			viewForAnnotation?.layer.borderWidth = 2
		}

//		Don't work =(
//		pinView.leftCalloutAccessoryView = self.pinLeftView(annotation as! AMPersonMapAnnotation)

		return viewForAnnotation!
	}

	func pinRightButton(annotation: AMPersonMapAnnotation) -> UIButton {
		let button = AMMainMapPinButton(type: .DetailDisclosure)
		button.annotation = annotation;
		button.addTarget(self, action: "pinRightButtonTapped:", forControlEvents: .TouchUpInside)

		return button
	}

	func pinLeftView(annotation: AMPersonMapAnnotation) -> UIView? {
		if let imageUrl = annotation.person.photo50 {
			let imageView = UIImageView()
			imageView.backgroundColor = UIColor.redColor()
			imageView.sd_setImageWithURL(NSURL(string: imageUrl)!)
			imageView.snp_makeConstraints(closure: { (make) -> Void in
				make.height.equalTo(20).priorityHigh()
				make.width.equalTo(20).priorityHigh()
			})
			return imageView
		} else {
			return nil
		}
	}

	func pinRightButtonTapped(sender :UIButton) {
		self.lastChoosenPerson = (sender as! AMMainMapPinButton).annotation?.person
		self.performSegueWithIdentifier("Profile", sender: self)
	}
}
