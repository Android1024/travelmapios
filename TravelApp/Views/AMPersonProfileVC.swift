//
//  AMPersonProfileVC.swift
//  TravelApp
//
//  Created by Мухаметзянов Айдар Азатович on 05/12/15.
//  Copyright © 2015 Aydar Mukhametzyanov. All rights reserved.
//

import UIKit

class AMPersonProfileVC: UIViewController, UITableViewDataSource, UITableViewDelegate {

	var tableView = UITableView()
	var avaImageView = UIImageView()

	var personId :Int? {
		didSet {
			if let newId = self.personId {
				self.viewModel = AMPersonProfileVM(id: newId)
			}
		}
	}

	var viewModel :AMPersonProfileVM? {
		didSet {
			self.viewModel?.rac_valuesForKeyPath("model",
				observer: viewModel)
				.distinctUntilChanged()
				.subscribeNext {[weak self] (_) -> Void in
					guard let sself = self else {
						return
					}

					sself.reloadUI()
			}
		}
	}

	override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
		super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
		self.navigationController?.title = "Profile"
	}

	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		self.navigationController?.title = "Profile"
	}

    override func viewDidLoad() {
        super.viewDidLoad()

		self.title = "Profile"

		self.view.addSubview(self.tableView)
		self.tableView.snp_makeConstraints { (make) -> Void in
			make.edges.equalTo(self.view)
		}
		self.tableView.delegate = self
		self.tableView.dataSource = self
		self.tableView.contentInset = UIEdgeInsets(top: 240, left: 0, bottom: 0, right: 0)
		self.tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "ProfileCell")

		self.view.addSubview(self.avaImageView)
		self.avaImageView.clipsToBounds = true
		self.avaImageView.contentMode = .ScaleAspectFill
		self.avaImageView.layer.cornerRadius = 100
		self.avaImageView.snp_makeConstraints { (make) -> Void in
			make.height.equalTo(200)
			make.width.equalTo(200)
			make.centerX.equalTo(self.view)
			make.top.equalTo(self.view.snp_top).offset(40 + self.navigationController!.navigationBar.frame.height)
		}
    }

	deinit {
		self.tableView.delegate = nil
		self.tableView.dataSource = nil
	}

	func reloadUI() {
		if let imageUrl = self.viewModel?.model?.photo200 {
			if (self.avaImageView.image == nil) {

				self.avaImageView.sd_setImageWithURL(NSURL(string: imageUrl),
					completed: {[weak self] (image, error, type, url) -> Void in
						guard let sself :AMPersonProfileVC = self else {
							return
						}

						sself.reloadUI()
				})
			} else {
				self.avaImageView.sd_setImageWithURL(NSURL(string: imageUrl))
			}
		}
		self.tableView.reloadData()
		self.tableView.setContentOffset(CGPoint(x: 0, y: -self.tableView.contentInset.top), animated: false)

	}

	// MARK: TableViewDelegate

	func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return 11
	}

	func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCellWithIdentifier("ProfileCell", forIndexPath: indexPath)
		cell.selectionStyle = .None
		if let model = self.viewModel?.model {
			var font = UIFont(name: "HelveticaNeue", size: 16)
			switch indexPath.row {
			case 0:
				font = UIFont(name: "HelveticaNeue-Bold", size: 20)
				cell.textLabel?.text = "\(model.firstname) \(model.lastname)"
			case 1:
				cell.textLabel?.text = "Email: \(model.email)"
			case 2:
				cell.textLabel?.text = "Birthday: \(model.birthday)"
			case 3:
				cell.textLabel?.text = "City: \(model.fromCity)"
			case 4:
				cell.textLabel?.text = "Status: \(model.status)"
			case 5:
				cell.textLabel?.text = "Job: \(model.jobs)"
			case 6:
				var s = "YES"
				if !model.help {
					s = "NO"
				}
				cell.textLabel?.text = "Need help: \(s)"
			case 7:
				let now = NSDate()
				let timeInterval = now.timeIntervalSince1970
				let delta = Double(model.lasttime) - timeInterval
				let date = now.dateByAddingTimeInterval(delta)
				cell.textLabel?.text = "Last time: \(date)"
			case 8:
				cell.textLabel?.text = "About: \(model.about)"
			case 9:
				cell.textLabel?.text = "Phone: \(model.phone)"
			case 10:
//				cell.textLabel?.text = "Time trip: \(model.timeTrip)"
//			case 11:
				cell.textLabel?.text = "Language: \(model.lang)"
			default:
				break
			}
			cell.textLabel?.font = font
		} else {
			cell.textLabel?.text = ""
		}
		return cell
	}

	// MARK: ScrollViewDelegate

	func scrollViewDidScroll(scrollView: UIScrollView) {
		if let _ = self.avaImageView.image {
			if scrollView.contentOffset.y + scrollView.contentInset.top > 0 {
				let shift = CGFloat(scrollView.contentOffset.y + scrollView.contentInset.top)

				let imageHeight = CGFloat(100.0)
				let scale = CGFloat((imageHeight - (shift / 2)) / imageHeight)

				if scale > 0.01 {
					self.avaImageView.hidden = false
					var transform = CGAffineTransformMakeTranslation(0, -shift / 2)
					transform = CGAffineTransformScale(transform, scale, scale)
					self.avaImageView.transform = transform
				} else {
					self.avaImageView.hidden = true
				}
			} else {
				self.avaImageView.hidden = false
				let shift = (-scrollView.contentOffset.y - scrollView.contentInset.top) / 2
				self.avaImageView.transform = CGAffineTransformMakeTranslation(0, shift)
			}
		}
	}

}
