//
//  AMTabBarController.swift
//  TravelApp
//
//  Created by Мухаметзянов Айдар Азатович on 06/12/15.
//  Copyright © 2015 Aydar Mukhametzyanov. All rights reserved.
//

import UIKit

class AMTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

		self.tabBar.tintColor = UIColor.blueColor()
		for vc in self.viewControllers! {
			print(vc.title)
		}
    }


}
