import UIKit
import MapKit

public class AMGeoObjectMapAnnotation: NSObject, MKAnnotation {

	let geoObject :AMGeoObject

	init(geoObject: AMGeoObject) {
		self.geoObject = geoObject
	}

	public var coordinate: CLLocationCoordinate2D {
		get {
			return self.geoObject.coordinate
		}
	}

	public var title: String? {
		get {
			return self.geoObject.name
		}
	}

	public var subtitle: String? {
		get {
			return nil
		}
	}
}
