import CoreLocation

class AMGeoObject: NSObject {

	let name :String
	let coordinate :CLLocationCoordinate2D
	let	id :String
	let imageUrl :String

	init(name :String,
		coordinate :CLLocationCoordinate2D,
		id :String,
		imageUrl :String) {
			self.name = name
			self.coordinate = coordinate
			self.id = id
			self.imageUrl = imageUrl
	}
	
}
