import UIKit
import MapKit

public class AMPersonMapAnnotation: NSObject, MKAnnotation {

	let person :AMPerson

	init(person: AMPerson) {
		self.person = person
	}

	public var coordinate: CLLocationCoordinate2D {
		get {
			return self.person.coordinate!
		}
	}

	public var title: String? {
		get {
			if self.person.firstname.characters.count > 0 ||
				self.person.lastname.characters.count > 0 {
					return "\(self.person.firstname) \(self.person.lastname)"
			} else {
				return nil
			}
		}
	}

	public var subtitle: String? {
		get {
			return nil
		}
	}
}
