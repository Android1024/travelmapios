import CoreLocation

class AMPerson: NSObject {

	var id :Int// Id пользователя системы

	var coordinate :CLLocationCoordinate2D?

	var status :String = ""// status да ладно?)
	var firstname :String = "" // Имя
	var email :String = "test@gmail.com" // Email addres user
	var age :Int? //Сколько лет
	var profilePhoto :Int?
	var startTrip :String? // начальная точка путешествия
	var endTrip :String? // конечная точка путешествия
	var help :Bool = false // позвал ли он напомощь
	var lasttime :Int = 0// Время последнего пришествия.
	var distanceTo :Float?//Растояние до меня.
	var visible :Bool?// Отображаемость на карте, в зависимости от настроек
	var lastname :String  = ""; // Фамилия
	var birthday :String  = "1964-08-19"; // дата! рождения (long)// todo
	var about :String = ""// дополнительная информация о пользователи
	var photo50 :String?//50*50
	var photo100 :String?//100*100
	var photo200 :String?//Аватарка размером 200*200
	var fromCity :String = "Moscow"// Город проживания пользователя
	var timeStart :String?//Время начала путешествия.
	var vkId :String?// Id из Вконтакте
	var fbId :String?
	var twId :String?
	var gender :Int? // 1 - мужик, 2 - женщина, 0 - неопределен
	var study :String?//место учебы
	var jobs :String = "Freelance"// место работы
	var phone :String = "+79137329932" // number phone
	var skype :String? // адрес по скайпу
	var timeTrip :String = "" // время в путeшествие
	var lang :String = "ru"

	init(personId: Int) {
		id = personId
	}

	func distanceToPerson(person :AMPerson) -> CLLocationDistance {
		let currLoc = CLLocation(latitude: self.coordinate!.latitude, longitude: self.coordinate!.longitude)
		let loc = CLLocation(latitude: person.coordinate!.latitude, longitude: person.coordinate!.longitude)

		return currLoc.distanceFromLocation(loc);
	}

}
