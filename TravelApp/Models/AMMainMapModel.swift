import UIKit

class AMMainMapModel: NSObject {

	let personsArray :[AMPerson]
	let geoObjects :[AMGeoObject]

	init(personsArray :[AMPerson], geoObjects :[AMGeoObject]) {
		self.personsArray = personsArray
		self.geoObjects = geoObjects
	}
}
