import CoreLocation

class AMCurrentStateModel: NSObject {

	let coordinates :CLLocationCoordinate2D?
	let nearestPersons :[AMPerson]?
	let nearestGeoObject :[AMGeoObject]?

	init(coordinates :CLLocationCoordinate2D?,
		nearestPersons :[AMPerson]?,
		nearestGeoObject :[AMGeoObject]?) {

			self.coordinates = coordinates
			self.nearestGeoObject = nearestGeoObject
			self.nearestPersons = nearestPersons
	}

}
