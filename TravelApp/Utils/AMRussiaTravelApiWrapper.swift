
import UIKit
import Alamofire
import SWXMLHash
import CoreLocation

extension String  {
	var md5: String! {
		let str = self.cStringUsingEncoding(NSUTF8StringEncoding)
		let strLen = CC_LONG(self.lengthOfBytesUsingEncoding(NSUTF8StringEncoding))
		let digestLen = Int(CC_MD5_DIGEST_LENGTH)
		let result = UnsafeMutablePointer<CUnsignedChar>.alloc(digestLen)

		CC_MD5(str!, strLen, result)

		let hash = NSMutableString()
		for i in 0..<digestLen {
			hash.appendFormat("%02x", result[i])
		}

		result.dealloc(digestLen)

		return String(format: hash.copy() as! String)
	}
}

class AMRussiaTravelApiWrapper: NSObject {

	let russiaTravelApiBaseURLString = "http://api.russia.travel/"
	let key = "nmVTayH7"
	let login = "aydarmukh"
	let password = "ayttravel"
	let apiHash = "nmVTayH7.9b47be054064f6aa4f18b7c99c513404"

	func requestObjectsAround(coordinate :CLLocationCoordinate2D, success:(geoObjects :[AMGeoObject], tocken: Int) -> Void,
		fail:(error :NSError, tocken: Int) -> Void) -> Int {
		return self.performRequest(self.requestXMLObjectsAround(coordinate, radius: 5), success: { (response, tocken) -> Void in
			if let xmlIndexer = response {
				let parsedResponse = self.parseAroundObjects(xmlIndexer)
				success(geoObjects: parsedResponse, tocken: tocken)
			} else {
				let error = NSError(domain: "Api", code: 0, userInfo: ["description" : "Invalid xml response"])
				fail(error: error, tocken: tocken)
			}
			}) { (error, tocken) -> Void in
				fail(error: error, tocken: tocken)
		}
	}

	func parseAroundObjects(xmlStr :XMLIndexer) -> [AMGeoObject] {

		var geoObjects = [AMGeoObject]()
		let itemsArray = xmlStr["response"]["items"]
		for item in itemsArray.children {

			var coordinate :CLLocationCoordinate2D?
			var imageUrl :String?
			var id :String?
			let name = item["name"]["text"].element?.text

			if let attr = item.element?.attributes {


				if let geo = attr["geo"] {

					let geoSplitted = geo.componentsSeparatedByString(",")
					if geoSplitted.count == 2 {
						if let lat = Double(geoSplitted[0]),
							let lon = Double(geoSplitted[1]) {
								coordinate = CLLocationCoordinate2D(latitude: lat, longitude: lon)
						}
					}
				}

				imageUrl = attr["image"]
				id = attr["id"]
			}

			if (imageUrl != nil && id != nil && name != nil && coordinate != nil) {
				let geoObject = AMGeoObject(name: name!,
					coordinate: coordinate!,
					id: id!,
					imageUrl: imageUrl!)

				geoObjects.append(geoObject)
			}
		}

		return geoObjects
	}

	func performRequest(requestXMLString: String,
		success:(response :XMLIndexer?, tocken :Int) -> Void,
		fail:(error :NSError, tocken :Int) -> Void) -> Int {

			let tocken = AMRequestTockenProvider.getNewTocken()

			Alamofire.request(
				.POST,
				russiaTravelApiBaseURLString,
				parameters: ["login" : login, "hash" : apiHash, "xml" : requestXMLString],
				encoding: .URL,
				headers: nil).responseString { [weak self] (let response :Response<String, NSError>) -> Void in
					guard let sself: AMRussiaTravelApiWrapper = self else {
						return
					}

					sself.calculateApiHash();// ayay useless

					if let error = response.result.error {
						print("performRequest error: \(requestXMLString) error: \(error)")
						fail(error: error, tocken: tocken);
					}
					else if let value = response.result.value {
//						print("performRequest success: \(requestXMLString) value: \(value)")
						let xmlRootObject: XMLIndexer? = SWXMLHash.parse(value);
						success(response: xmlRootObject, tocken: tocken)
					}
			}

			return tocken
	}

	func calculateApiHash() -> String {
		let keyPass = String(format: "%@.%@", key, password)
		return String(format: "%@.%@", key, keyPass.md5)
	}

	func requestXMLObjectsAround(coordinate: CLLocationCoordinate2D, radius: Float) -> String {
		return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
			"<request action=\"get-objects-for-update\" >" +
			"<point radius=\"\(radius)\">\(coordinate.latitude),\(coordinate.longitude)</point>" +
			"<attributes>" +
			"<addressRegion />" +
			"<photo/>" +
			"<type/>" +
			"<addressArea/>" +
			"<addressLocality/>" +
			"<streetAddress/>" +
			"<name/>" +
			"<url/>" +
			"<geo/>" +
			"<telephone/>" +
			"<startDate/>" +
			"<endDate/>" +
			"<review/>" +
			"<addressRegion/>" +
			"</attributes>" +
			"</request>"
	}

	func requestXMLObjectsTypesDictionary() -> String {
		return "<?xml version=\"1.0\" encoding=\"UTF-8\"?> <request action=\"get-library\" type=\"object-type\" />"
	}

	func requestXMLObjectsTypesGroupsDictionary() -> String {
		return "<?xml version=\"1.0\" encoding=\"UTF-8\"?> <request action=\"get-library\" type=\"type-group\" />"
	}

	func requestXMLRegionsForGeoAttachingDictionary() -> String {
		return "<?xml version=\"1.0\" encoding=\"UTF-8\"?> <request action=\"get-library\" type=\"addressRegion\" />"
	}

	func requestXMLObjectsOfRegionsForGeoAttachingDictionary() -> String {
		return "<?xml version=\"1.0\" encoding=\"UTF-8\"?> <request action=\"get-library\" type=\"addressLocality\" />"
	}

	func requestXMLObjectsServicesDictionary() -> String {
		return "<?xml version=\"1.0\" encoding=\"UTF-8\"?> <request action=\"get-library\" type=\"services\" />"
	}

}
