import UIKit
import Alamofire
import CoreLocation

class TravelMapApiError: ErrorType {
	var comment :String?

	init(comment: String) {
		self.comment = comment
	}
}

class AMTravelMapApiWrapper: NSObject {

	let apiURL = "http://travelmap.su/api/v1/users"

	func performRequest(success:(personArray :[AMPerson]?, tocken: Int) -> Void,
		fail:(error :NSError, tocken: Int) -> Void) -> Int {

			let tocken = AMRequestTockenProvider.getNewTocken()

			Alamofire.request(
				.GET,
				apiURL,
				encoding: .URL,
				headers: nil).responseString { [weak self] (let response :Response<String, NSError>) -> Void in
					guard let sself: AMTravelMapApiWrapper = self else {
						return
					}

					if let error = response.result.error {
						print("performRequest error: \(error)")
						fail(error: error, tocken: tocken);
					}
					else if let value = response.result.value {
//						print("performRequest success: \(value)")

						if let personArray = sself.parseResponse(value) {
							success(personArray: personArray, tocken: tocken)
						}
						else {
							let error = NSError(domain: "Api", code: 0, userInfo: ["description" : "Can't parse"])
							print("performRequest error: \(error)")
							fail(error: error, tocken: tocken)
						}
					}
			}
			
			return tocken
	}

	//	"about": "",
	//	"birthday": "0000-00-00",
	//	"data_reg": "0000-00-00 00:00:00",
	//	"fbid": "0",
	//	"firstname": "\u041a\u043e\u043b\u044f",
	//	"from_city": "\u0421\u043d\u0435\u0436\u0438\u043d\u0441\u043a",
	//	"gender": 0,
	//	"id": 12,
	//	"jobs": "",
	//	"lang": "0",
	//	"lastname": "\u0421\u0438\u0434\u043e\u0440\u043e\u0432",
	//	"lasttime": 1446209456,
	//	"latitude": 41.8719,
	//	"longitude": 12.5674,
	//	"phone": "",
	//	"photo100": "https:\/\/pp.vk.me\/c622022\/v622022512\/49c9f\/qJ_wWDiHTVY.jpg",
	//	"photo200": "https:\/\/pp.vk.me\/c622022\/v622022512\/49c9c\/-HXyDJBka2A.jpg",
	//	"": "https:\/\/pp.vk.me\/c622022\/v622022512\/49c9f\/qJ_wWDiHTVY.jpg",
	//	"skype": "",
	//	"status": "",
	//	"study": "",
	//	"time_trip": "00:00:00",
	//	"twitter": "",
	//	"user_email": "Anoimuo",
	//	"visible": "0",
	//	"vkid": "0"

	private func parseResponse(responseJson: NSString) -> [AMPerson]? {
		do {
			if let jsonData = responseJson.dataUsingEncoding(NSUTF16StringEncoding) {
				let json = try NSJSONSerialization.JSONObjectWithData(jsonData, options: .AllowFragments)

				if let jsonArray = json as? [[String: AnyObject]] {
					var personArray = [AMPerson]()

					for personJson in jsonArray {
						if let id = personJson["id"] as? Int {
							let person = AMPerson(personId: id)
							personArray.append(person)

							if let about = personJson["about"] as? String {
								person.about = about
							}

							if let birthday = personJson["birthday"] as? String {
								if birthday != "0000-00-00" {
									person.birthday = birthday
								}
							}

							if let fbid = personJson["fbid"] as? String {
								person.fbId = fbid
							}

							if let firstname = personJson["firstname"] as? String {
								person.firstname = firstname
							}

							if let from_city = personJson["from_city"] as? String {
								person.fromCity = from_city
							}

							if let gender = personJson["gender"] as? Int {
								person.gender = gender
							}

							if let jobs = personJson["jobs"] as? String {
								if jobs.characters.count > 0 {
									person.jobs = jobs
								}
							}

							if let lastname = personJson["lastname"] as? String {
								person.lastname = lastname
							}

							if let lasttime = personJson["lasttime"] as? Int {
								person.lasttime = lasttime
							}

							if let latitude = personJson["latitude"] as? Double ,
								let longitude = personJson["longitude"] as? Double {
								person.coordinate = CLLocationCoordinate2D(latitude: latitude,
									longitude: longitude)
							}

							if let phone = personJson["phone"] as? String {
								if phone.characters.count > 0 {
									person.phone = phone
								}
							}

							if let photo100 = personJson["photo100"] as? String {
								person.photo100 = photo100
							}

							if let photo200 = personJson["photo200"] as? String {
								person.photo200 = photo200
							}

							if let photo50 = personJson["photo50"] as? String {
								person.photo50 = photo50
							}

							if let skype = personJson["skype"] as? String {
								person.skype = skype
							}

							if let study = personJson["study"] as? String {
								person.study = study
							}

							if let time_trip = personJson["time_trip"] as? String {
								person.timeTrip = time_trip
							}

							if let twitter = personJson["twitter"] as? String {
								person.twId = twitter
							}

							if let user_email = personJson["user_email"] as? String {
								person.email = user_email
							}

							if let visible = personJson["visible"] as? Bool {
								person.visible = visible
							}

							if let vkid = personJson["vkid"] as? String {
								person.vkId = vkid
							}

							if let status = personJson["status"] as? String {
								person.status = status
							}
						}
					}
					return personArray
				}
				else {
					throw TravelMapApiError(comment: "Can't find array")
				}
			}
		} catch {
			print("error serializing JSON: \(error)")
		}

		return nil
	}

}
