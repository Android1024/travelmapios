import Foundation
import ReactiveCocoa

class AMDataProvider: NSObject {

	static let sharedInstance = AMDataProvider()
	static let currentUserHardCodedId = 107

	private var personsArray :[AMPerson]?
	private var geoObjectsArray :[AMGeoObject]?

	private let travelMapApiWrapper = AMTravelMapApiWrapper()
	private let russianTravelApiWrapper = AMRussiaTravelApiWrapper()

	private var loadingPersonTocken :Int = -1
	private var loadingGeoObjectsTocken :Int = -1

	let personsArrayUpdatedNotificationName = "AMDataProviderPersonsArrayUpdatedNotificationName"
	let personsArrayFailedNotificationName = "AMDataProviderPersonsArrayFailedNotificationName"
	let personsArrayNotificationTockenKey = "AMDataProviderPersonsArrayNotificationTockenKey"

	let geoObjectsArrayUpdatedNotificationName = "AMDataProviderGeoObjectsArrayUpdatedNotificationName"
	let geoObjectsArrayFailedNotificationName = "AMDataProviderGeoObjectsArrayFailedNotificationName"
	let geoObjectsArrayNotificationTockenKey = "AMDataProviderGeoObjectsArrayNotificationTockenKey"

	func getPersonsArray() -> (Int, [AMPerson]?) {

		guard let _ = personsArray else {

			if self.loadingPersonTocken < 0 {
				self.loadingPersonTocken  = travelMapApiWrapper.performRequest({[weak self] (personArray, tocken) -> Void in
					guard let sself = self else {
						return;
					}

					if tocken == sself.loadingPersonTocken {
						sself.loadingPersonTocken = -1;
					}

					sself.personsArray = personArray
					NSNotificationCenter.defaultCenter().postNotificationName(sself.personsArrayUpdatedNotificationName,
						object: sself.personsArray,
						userInfo: [sself.personsArrayNotificationTockenKey : tocken])
					}, fail: {[weak self] (error, tocken) -> Void in
						guard let sself = self else {
							return;
						}

						if tocken == sself.loadingPersonTocken {
							sself.loadingPersonTocken = -1;
						}

						NSNotificationCenter.defaultCenter().postNotificationName(sself.personsArrayFailedNotificationName,
							object: nil,
							userInfo: [sself.personsArrayNotificationTockenKey : tocken])
				})

				return (self.loadingPersonTocken, nil)
			}

			return (self.loadingPersonTocken, nil)
		}

		return (-1, personsArray)
	}

	func getGeoObjectsArray(coordinate :CLLocationCoordinate2D) -> (Int, [AMGeoObject]?) {

		guard let _ = geoObjectsArray else {

			if self.loadingGeoObjectsTocken < 0 {
				self.loadingGeoObjectsTocken  = russianTravelApiWrapper.requestObjectsAround(coordinate, success: {[weak self] (geoObjectsArray, tocken) -> Void in
					guard let sself = self else {
						return;
					}

					if tocken == sself.loadingPersonTocken {
						sself.loadingPersonTocken = -1;
					}

					sself.geoObjectsArray = geoObjectsArray
					NSNotificationCenter.defaultCenter().postNotificationName(sself.geoObjectsArrayUpdatedNotificationName,
						object: sself.geoObjectsArray,
						userInfo: [sself.geoObjectsArrayNotificationTockenKey : tocken])
					}, fail: {[weak self] (error, tocken) -> Void in
						guard let sself = self else {
							return;
						}

						if tocken == sself.loadingGeoObjectsTocken {
							sself.loadingGeoObjectsTocken = -1;
						}

						NSNotificationCenter.defaultCenter().postNotificationName(sself.geoObjectsArrayFailedNotificationName,
							object: nil,
							userInfo: [sself.geoObjectsArrayNotificationTockenKey : tocken])
					})

				return (self.loadingGeoObjectsTocken, nil)
			}

			return (self.loadingGeoObjectsTocken, nil)
		}
		
		return (-1, geoObjectsArray)

	}

}
