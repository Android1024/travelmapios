import UIKit

class AMRequestTockenProvider: NSObject {
	private static let sharedInstance = AMRequestTockenProvider()
	private var tocken = 1;

	static func getNewTocken() -> Int {
		let old = self.sharedInstance.tocken
		self.sharedInstance.tocken = self.sharedInstance.tocken + 1
		return old
	}
}
